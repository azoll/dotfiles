# My Enhanced Dotfiles 🐧

Welcome to my dotfiles repository, efficiently managed using [chezmoi](https://www.chezmoi.io) across a variety of machines while ensuring security.

## Installation 🚀

Getting started is a breeze! To install chezmoi and set up everything seamlessly, execute the following command in your terminal:

```bash
sh -c "$(curl -fsLS get.chezmoi.io)" -- init --apply https://gitlab.com/azoll/dotfiles
```
## How to Customize 🎨

Feel free to personalize your dotfiles by following these simple steps:

1. **Make Updates**: Modify your dotfiles and introduce new files using `chezmoi add <file/dir name>`.

2. **Explore Changes**: Check out the differences with `chezmoi diff`.

3. **Apply Modifications**: Apply the changes confidently using `chezmoi apply`.

4. **Commit and Push**: Save your changes to git and push them to the repository.

### Additional Customization Steps 🛠️

Expand your dotfiles customization with these extra tips:

1. **Managing Secrets**: Safely handle sensitive information using chezmoi's secret management features. Run `chezmoi edit-secret` to access encrypted secrets.

2. **Template Rendering**: Utilize chezmoi's template rendering to dynamically generate configurations. Add templates to your dotfiles, and chezmoi will handle the rest.

3. **Sharing Configurations**: Share your dotfiles with others by forking or cloning your repository. Make it easy for collaborators to apply your configurations on their machines.

4. **Conditional Configurations**: Tailor your dotfiles based on machine-specific conditions. chezmoi supports conditionals in configurations, allowing you to adapt settings based on the environment.

Embrace the power of chezmoi to customize, share, and manage your dotfiles effortlessly! 🚀